import { combineReducers } from "redux";//combine all your reducers combineReducers function
import modalsReducer from "./modalsReducer";
import validationsReducer from "./validationsReducer";
import tweetsReducer from "./tweetsReducer";

export default combineReducers({
  modals: modalsReducer,
  validations: validationsReducer,
  tweets: tweetsReducer
});
