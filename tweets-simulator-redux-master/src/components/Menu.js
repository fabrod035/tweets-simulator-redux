import React from "react";
import { Container, Navbar, Button } from "react-bootstrap";
import { useDispatch } from "react-redux";//actions
import { openCloseAddTweetModalAction } from "../actions/modalsActions";
import LogoRedux from "../assets/img/redux.png";

export default function Menu() {
  // Dispatch to execute actions  modalsActions/openCloseAddTweetModal function
  const dispatch = useDispatch();
  const openCloseAddTweetModal = state =>
    dispatch(openCloseAddTweetModalAction(state));

    //when the button gets clicked ,openModal get's called and opens the Modal on line 31
  const openModal = () => {
    openCloseAddTweetModal(true);
  };

  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand>
          <img
            alt="Tweets Simulator Redux"
            src={LogoRedux}
            width="30"
            height="30"
            className="d-inline-block aling-top mr-4"
          />
          Tweets Simulator REDUX
        </Navbar.Brand>
        <Button variant="outline-success" onClick={openModal}>
          New Tweet
        </Button>
      </Container>
    </Navbar>
  );
}
