import React from "react";
import { Modal as ModalB } from "react-bootstrap";
//our modal has the same name as our component /change  as ModalB
import { useDispatch, useSelector } from "react-redux";
import { openCloseAddTweetModalAction } from "../actions/modalsActions";

export default function Modal(props) {
  const { children } = props;

  // Dispatch and execute  actions
  const dispatch = useDispatch();
  const closeModal = state => dispatch(openCloseAddTweetModalAction(state));

  // access the  store
  const isOpenModal = useSelector(state => state.modals.stateModalAddTweet);

  return (
    <ModalB
      show={isOpenModal}
      onHide={() => closeModal(false)}
      size="lg"
      centered
    >
      {children}
    </ModalB>
  );
}
